//
//  ViewController.swift
//  OCR-Sample
//
//  Created by Waqas Ali on 12/2/19.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var dataArray = [CardData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupTableView()
       let ocrReader = OCRReader()
        //ocrReader.performOCR(on: url(for: .document), recognitionLevel: .accurate)
      showSpinner()
      ocrReader.performOCR(on: UIImage(named: "sample")!, recognitionLevel: .accurate) { [weak self] (cardArray) in
              self?.hideSpinner()
             self?.dataArray = cardArray
             self?.tableView.reloadData()
              print("completion",cardArray )
        }
       
    }

    func setupTableView() {
        
        tableView.register(UINib.init(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func showSpinner() {
        spinner.isHidden = false
        tableView.isHidden = true
        spinner.startAnimating()
    }
    
    func hideSpinner() {
         spinner.stopAnimating()
         spinner.isHidden = true
         tableView.isHidden = false
    }
}


extension ViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        cell.setCellData(data: dataArray[indexPath.row])
        return cell
    }
    
    
}

extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}



