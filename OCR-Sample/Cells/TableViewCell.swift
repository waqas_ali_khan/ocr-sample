//
//  TableViewCell.swift
//  OCR-Sample
//
//  Created by Waqas Ali on 12/5/19.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPurpose: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(data : CardData){
        lblDate.text = data.date
        lblAmount.text = data.amount
        lblPurpose.text = data.purpose
        lblCurrency.text = data.currency
        var category = data.category
        if let range = category.range(of: "Category: ") {
           category.removeSubrange(range)
        }
         lblCategory.text = category
    }
    
}
