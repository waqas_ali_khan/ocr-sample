//
//  OCRReader.swift
//  OCR-Sample
//
//  Created by Waqas Ali on 12/5/19.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//

import UIKit
import Vision


enum DemoImage: String {
    case document = "sample"
    case licensePlate = "demoLicensePlate"
}

class OCRReader {
  /*  func performOCR(on url: URL?, recognitionLevel: VNRequestTextRecognitionLevel)  {
        guard let url = url else { return }
        let requestHandler = VNImageRequestHandler(url: url, options: [:])
     
        let request = VNRecognizeTextRequest  { (request, error) in
            if let error = error {
                print(error)
                return
            }

            guard let observations = request.results as? [VNRecognizedTextObservation] else { return }

            for currentObservation in observations {
                let topCandidate = currentObservation.topCandidates(1)
                if let recognizedText = topCandidate.first {
                    print(recognizedText.string)
                }
            }
        }
        request.recognitionLevel = recognitionLevel
        request.usesLanguageCorrection = true
    

        try? requestHandler.perform([request])
    }
    
    */
    func performOCR(on img: UIImage, recognitionLevel: VNRequestTextRecognitionLevel, completion: (([CardData])->Void)?) {
        guard let image = img.cgImage else { return}
    
        let requestHandler = VNImageRequestHandler.init(cgImage: image, options: [:])
           let request = VNRecognizeTextRequest  { (request, error) in
               if let error = error {
                   print(error)
                   return
               }

               guard let observations = request.results as? [VNRecognizedTextObservation] else { return }
            
            let stringsToByPass = ["Transaction Details", "Doing Business", "Additional Information", "Description", "Price", "Reference"]
            var arrayOfResults = [String]()

            for (index,currentObservation) in observations.enumerated() {
              
                   let topCandidate = currentObservation.topCandidates(1)
                   if let recognizedText = topCandidate.first {
                       let str = recognizedText.string
                    if index > 12 && str.lowercased() != "amount" {
                let result = stringsToByPass.contains(where: str.contains)
                    if !result{
                        arrayOfResults.append(str)
                   }
                   }
                }
            }
            
          var finalResults = [String]()
           var iin = 0
            for val in arrayOfResults{
                if iin < 4{
                    finalResults.append(val)
                    iin += 1
                }else {
                    if val.contains("Category") ||  val.contains("Calegory"){
                         finalResults.append(val)
                        iin = 0
                    }
                }
            }
            
            let finalArray = finalResults.chunked(into: 5)
            
            var resultingArray = [CardData]()
            
            for arr in finalArray{
                if arr.count == 5 {
                    var cardData = CardData()
                    cardData.date = arr[0]
                    cardData.purpose = arr[1]
                    cardData.amount = arr[3]
                    cardData.category = arr[4]
                    resultingArray.append(cardData)
                }
            }
            //--ww  print("there", resultingArray)
            DispatchQueue.main.async {
                  completion?(resultingArray)
            }
          
           
           }
           request.recognitionLevel = recognitionLevel
           request.usesLanguageCorrection = true
            request.customWords = ["Amount", "Category"]
       
        DispatchQueue.global().async {
           try? requestHandler.perform([request])
        }
       
       }
}

func url(for image: DemoImage) -> URL? {
    return Bundle.main.url(forResource: image.rawValue, withExtension: "jpg")
}


extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
