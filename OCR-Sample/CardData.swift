//
//  CardData.swift
//  OCR-Sample
//
//  Created by Waqas Ali on 12/5/19.
//  Copyright © 2019 Waqas Ali. All rights reserved.
//



struct CardData{
    var date = ""
    var amount = ""
    var category = ""
    var purpose = ""
    var paymentMethod = "Credit Card"
    var currency = "US dollar"
}
